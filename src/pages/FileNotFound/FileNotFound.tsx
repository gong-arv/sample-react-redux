import styles from './FileNotFound.module.css'

const FileNotFoundPage = () => {
  return (
    <div className={styles['container']}>File Not Found!</div>
  )
}

export default FileNotFoundPage
