import ReactDOM from 'react-dom'
import React, { Suspense } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import 'index.css'
import { Loading } from 'pages'
import routes from 'routes'
import { store } from 'redux/store'
import { Provider } from 'react-redux'
import * as serviceWorker from 'serviceWorker'

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Suspense fallback={<Loading />}>
        <BrowserRouter>
          <Switch>
            {routes.map((route, index) => <Route key={index} {...route} />)}
          </Switch>
        </BrowserRouter>
      </Suspense>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
