import React from 'react'
import { RouteProps } from 'react-router-dom'

const lazyComponent = (page: string) => {
  const Component = React.lazy(() => import(`pages/${page}`))
  return () => <Component />
}

const routes: RouteProps[] = [
  { path: '/', exact: true, component: lazyComponent('Home') },
  { path: '/', component: lazyComponent('FileNotFound') },
]

export default routes
